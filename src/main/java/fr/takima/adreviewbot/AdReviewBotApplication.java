package fr.takima.adreviewbot;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.model.ValidationResult;
import fr.takima.adreviewbot.service.AdReaderService;
import fr.takima.adreviewbot.validation.ad.AdValidator;

import java.io.IOException;
import java.util.List;

public class AdReviewBotApplication {
    public static void main(String[] args) throws IOException {
        Ad ad = AdReaderService.INSTANCE.readAd("ad-sample.json");
        List<String> errors = AdValidator.INSTANCE.validate(ad);
        ValidationResult validationResult = ValidationResult.builder()
                                                            .reference(ad.getReference())
                                                            .scam(!errors.isEmpty())
                                                            .rules(errors)
                                                            .build();
        System.out.println(validationResult.toSIGOUT());
    }
}
