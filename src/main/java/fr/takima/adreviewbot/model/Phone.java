package fr.takima.adreviewbot.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Phone {
    private final String value;

    @JsonCreator
    private Phone(Builder builder) {
        this.value = builder.value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Phone{" +
               "value='" + value + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Phone phone = (Phone) o;
        return Objects.equals(value, phone.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        @JsonProperty
        private String value;

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public Phone build() {
            return new Phone(this);
        }
    }
}
