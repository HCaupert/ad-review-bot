package fr.takima.adreviewbot.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

public class Ad {
    private final LocalDateTime creationDate;
    private final int price;
    private final Set<String> publicationOptions;
    private final String reference;
    private final Item item;
    private final Contact contact;

    @JsonCreator
    private Ad(Builder builder) {
        this.creationDate = builder.creationDate;
        this.price = builder.price;
        this.publicationOptions = builder.publicationOptions;
        this.reference = builder.reference;
        this.item = builder.item;
        this.contact = builder.contact;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public int getPrice() {
        return price;
    }

    public Set<String> getPublicationOptions() {
        return publicationOptions;
    }

    public String getReference() {
        return reference;
    }

    public Item getItem() {
        return item;
    }

    public Contact getContacts() {
        return contact;
    }

    @Override
    public String toString() {
        return "Ad{" +
               "creationDate=" + creationDate +
               ", price=" + price +
               ", publicationOptions='" + publicationOptions + '\'' +
               ", reference='" + reference + '\'' +
               ", item='" + item + '\'' +
               ", contact='" + contact + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Ad ad = (Ad) o;
        return price == ad.price &&
               Objects.equals(creationDate, ad.creationDate) &&
               Objects.equals(publicationOptions, ad.publicationOptions) &&
               Objects.equals(reference, ad.reference) &&
               Objects.equals(item, ad.item) &&
               Objects.equals(contact, ad.contact);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creationDate, price, publicationOptions, reference, item, contact);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        @JsonProperty
        private LocalDateTime creationDate;
        @JsonProperty
        private int price;
        @JsonProperty
        private Set<String> publicationOptions;
        @JsonProperty
        private String reference;
        @JsonProperty
        private Item item;
        @JsonProperty(value = "contacts")
        private Contact contact;

        public Builder creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public Builder price(int price) {
            this.price = price;
            return this;
        }

        public Builder publicationOptions(Set<String> publicationOptions) {
            this.publicationOptions = publicationOptions;
            return this;
        }

        public Builder reference(String reference) {
            this.reference = reference;
            return this;
        }

        public Builder item(Item item) {
            this.item = item;
            return this;
        }

        public Builder contact(Contact contact) {
            this.contact = contact;
            return this;
        }

        public Ad build() {
            return new Ad(this);
        }
    }
}
