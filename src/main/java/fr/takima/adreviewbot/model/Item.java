package fr.takima.adreviewbot.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Item {
    private final String make;
    private final String model;
    private final String version;
    private final String category;
    private final String eanCode;

    @JsonCreator
    private Item(Builder builder) {
        this.make = builder.make;
        this.model = builder.model;
        this.version = builder.version;
        this.category = builder.category;
        this.eanCode = builder.eanCode;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getVersion() {
        return version;
    }

    public String getCategory() {
        return category;
    }

    public String getEanCode() {
        return eanCode;
    }

    @Override
    public String toString() {
        return "Item{" +
               "make='" + make + '\'' +
               ", model='" + model + '\'' +
               ", version='" + version + '\'' +
               ", category='" + category + '\'' +
               ", eanCode='" + eanCode + '\'' +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Item item = (Item) o;
        return Objects.equals(make, item.make) &&
               Objects.equals(model, item.model) &&
               Objects.equals(version, item.version) &&
               Objects.equals(category, item.category) &&
               Objects.equals(eanCode, item.eanCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(make, model, version, category, eanCode);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        @JsonProperty
        private String make;
        @JsonProperty
        private String model;
        @JsonProperty
        private String version;
        @JsonProperty
        private String category;
        @JsonProperty
        private String eanCode;

        public Builder make(String make) {
            this.make = make;
            return this;
        }

        public Builder model(String model) {
            this.model = model;
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder category(String category) {
            this.category = category;
            return this;
        }

        public Builder eanCode(String eanCode) {
            this.eanCode = eanCode;
            return this;
        }

        public Item build() {
            return new Item(this);
        }
    }
}
