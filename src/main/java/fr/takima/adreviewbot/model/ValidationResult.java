package fr.takima.adreviewbot.model;

import java.util.List;
import java.util.Objects;

public class ValidationResult {
    private final String reference;
    private final boolean scam;
    private final List<String> rules;

    public ValidationResult(Builder builder) {
        this.reference = builder.reference;
        this.scam = builder.scam;
        this.rules = builder.rules;
    }

    public String getReference() {
        return reference;
    }

    public boolean isScam() {
        return scam;
    }

    public List<String> getRules() {
        return rules;
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
               "reference='" + reference + '\'' +
               ", scam=" + scam +
               ", rules=" + rules +
               '}';
    }

    public String toSIGOUT() {
        return "{\n"
               + "\"reference\": " + reference + ",\n"
               + "\"scam\": " + scam + ",\n"
               + "\"rules\": " + rules + "\n"
               + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        ValidationResult that = (ValidationResult) o;
        return scam == that.scam &&
               Objects.equals(reference, that.reference) &&
               Objects.equals(rules, that.rules);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference, scam, rules);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String reference;
        private boolean scam;
        private List<String> rules;

        public Builder reference(String reference) {
            this.reference = reference;
            return this;
        }

        public Builder scam(boolean scam) {
            this.scam = scam;
            return this;
        }

        public Builder rules(List<String> rules) {
            this.rules = rules;
            return this;
        }

        public ValidationResult build() {
            return new ValidationResult(this);
        }
    }
}
