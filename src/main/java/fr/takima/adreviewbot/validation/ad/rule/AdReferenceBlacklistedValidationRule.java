package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.service.BlacklistService;
import fr.takima.adreviewbot.validation.ad.AdValidationRule;

import java.util.List;

public class AdReferenceBlacklistedValidationRule implements AdValidationRule {

    public static final String ruleName = "rule::reference::blacklist";

    @Override
    public List<String> validate(Ad ad) {
        return BlacklistService.INSTANCE.isAdBlackListed(ad) ?
               List.of(ruleName) :
               List.of();
    }
}
