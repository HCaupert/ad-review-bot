package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.service.QuotationService;
import fr.takima.adreviewbot.validation.ad.AdValidationRule;

import java.util.List;

public class AdPriceValidationRule implements AdValidationRule {

    public static final String ruleName = "rule::price::quotation_rate";

    @Override
    public List<String> validate(Ad ad) {
        int itemQuote = QuotationService.INSTANCE.calculateItemQuote(ad);
        if (ad.getPrice() < minimumItemPrice(itemQuote) || ad.getPrice() > maximumItemPrice(itemQuote)) {
            return List.of(ruleName);
        }
        return List.of();
    }

    private double minimumItemPrice(int itemQuote) {
        return 0.8 * itemQuote;
    }

    private double maximumItemPrice(int itemQuote) {
        return 1.2 * itemQuote;
    }
}
