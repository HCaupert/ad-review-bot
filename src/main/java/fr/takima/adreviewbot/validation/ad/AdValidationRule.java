package fr.takima.adreviewbot.validation.ad;

import fr.takima.adreviewbot.model.Ad;

import java.util.List;

public interface AdValidationRule {
    List<String> validate(Ad ad);
}
