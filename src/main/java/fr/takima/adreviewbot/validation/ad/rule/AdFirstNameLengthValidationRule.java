package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.validation.ad.AdValidationRule;

import java.util.List;

public class AdFirstNameLengthValidationRule implements AdValidationRule {

    public static final String ruleName = "rule::firstname::length";

    public static int minimumLength = 2;

    @Override
    public List<String> validate(Ad ad) {
        return ad.getContacts().getFirstName().length() > minimumLength ?
               List.of() :
               List.of(ruleName);
    }
}
