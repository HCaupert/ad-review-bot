package fr.takima.adreviewbot.validation.ad;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.validation.ad.rule.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AdValidator {
    public static AdValidator INSTANCE = new AdValidator();

    private AdValidator() {
    }

    private final Set<AdValidationRule> validationRules = Set.of(
            new AdEmailAlphaRateValidationRule(),
            new AdEmailNumberRateValidationRule(),
            new AdFirstNameLengthValidationRule(),
            new AdLastNameLengthValidationRule(),
            new AdPriceValidationRule(),
            new AdReferenceBlacklistedValidationRule()
    );

    public List<String> validate(Ad ad) {
        return validationRules.parallelStream()
                              .flatMap(adValidationRule -> adValidationRule.validate(ad).stream())
                              .collect(Collectors.toList());
    }
}
