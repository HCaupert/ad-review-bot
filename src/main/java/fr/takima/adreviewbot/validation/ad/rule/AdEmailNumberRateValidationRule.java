package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.validation.ad.AdValidationRule;

import java.util.List;

public class AdEmailNumberRateValidationRule implements AdValidationRule {

    public static final String ruleName = "rule:email:number_rate";

    public static final float maximumRate = 0.3f;

    @Override
    public List<String> validate(Ad ad) {
        String emailFirstPart = ad.getContacts().getEmail().split("@")[0];
        float numericElements = emailFirstPart.replaceAll("[^0-9]", "").length();

        return numericElements / emailFirstPart.length() < maximumRate ?
               List.of() :
               List.of(ruleName);
    }
}
