package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.validation.ad.AdValidationRule;

import java.util.List;

public class AdEmailAlphaRateValidationRule implements AdValidationRule {

    public static final String ruleName = "rule:email:alpha_rate";

    public static final float minimumRate = 0.7f;

    @Override
    public List<String> validate(Ad ad) {
        String emailFirstPart = ad.getContacts().getEmail().split("@")[0];
        float alphaNumericElements = emailFirstPart.replaceAll("[^A-Za-z0-9]", "").length();

        return alphaNumericElements / emailFirstPart.length() > minimumRate ?
               List.of() :
               List.of(ruleName);
    }
}
