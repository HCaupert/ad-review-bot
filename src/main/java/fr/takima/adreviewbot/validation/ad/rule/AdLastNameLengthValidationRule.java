package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.validation.ad.AdValidationRule;

import java.util.List;

public class AdLastNameLengthValidationRule implements AdValidationRule {

    public static final String ruleName = "rule::lastname::length";

    public static final int minimumLength = 2;

    @Override
    public List<String> validate(Ad ad) {
        return ad.getContacts().getLastName().length() > minimumLength ?
               List.of() :
               List.of(ruleName);
    }
}
