package fr.takima.adreviewbot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fr.takima.adreviewbot.model.Ad;

import java.io.File;
import java.io.IOException;

public class AdReaderService {

    public static final AdReaderService INSTANCE = new AdReaderService();

    private AdReaderService() {
        mapper.registerModule(new JavaTimeModule());
    }

    private final ObjectMapper mapper = new ObjectMapper();

    public Ad readAd(String filePath) throws IOException {
        return mapper.readValue(new File(filePath), Ad.class);
    }
}
