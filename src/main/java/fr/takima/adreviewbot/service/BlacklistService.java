package fr.takima.adreviewbot.service;

import fr.takima.adreviewbot.model.Ad;

public class BlacklistService {
    public static BlacklistService INSTANCE = new BlacklistService();

    private BlacklistService() {
    }

    public boolean isAdBlackListed(Ad ad) {
        //blacklistClientClient.isAdBlackListed(ad.getItem().getEanCode())...
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //
        return ad.getItem().getEanCode().equals("9-782940-199617");
    }
}
