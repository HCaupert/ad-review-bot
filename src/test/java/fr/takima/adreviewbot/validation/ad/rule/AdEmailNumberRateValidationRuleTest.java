package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.model.Contact;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AdEmailNumberRateValidationRuleTest {

    AdEmailNumberRateValidationRule validationRule = new AdEmailNumberRateValidationRule();

    @Test
    void shouldBeOk() {
        assertThat(validationRule.validate(buildAd("thisIsValid22@email.fr"))).isEmpty();
    }

    @Test
    void shouldBeInvalid() {
        assertThat(validationRule.validate(buildAd("invalid666@email.fr"))).containsOnly(AdEmailNumberRateValidationRule.ruleName);
    }

    Ad buildAd(String email) {
        return Ad.builder()
                 .contact(Contact.builder()
                                 .email(email)
                                 .build())
                 .build();
    }
}