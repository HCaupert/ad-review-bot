package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AdPriceValidationRuleTest {

    AdPriceValidationRule priceValidationRule = new AdPriceValidationRule();

    @Test
    void shouldBeOk() {
        assertThat(priceValidationRule.validate(Ad.builder().price(35000).build())).isEmpty();
    }

    @Test
    void shouldBeInvalidForTooLowPrice() {
        assertThat(priceValidationRule.validate(Ad.builder().price(0).build())).containsOnly(AdPriceValidationRule.ruleName);
    }

    @Test
    void shouldBeInvalidForTooHighPrice() {
        assertThat(priceValidationRule.validate(Ad.builder().price(100000).build())).containsOnly(AdPriceValidationRule.ruleName);
    }
}
