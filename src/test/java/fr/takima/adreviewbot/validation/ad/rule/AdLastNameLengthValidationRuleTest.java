package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.model.Contact;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AdLastNameLengthValidationRuleTest {

    AdLastNameLengthValidationRule validationRule = new AdLastNameLengthValidationRule();

    @Test
    void shouldBeOk() {
        assertThat(validationRule.validate(buildAd("a".repeat(AdLastNameLengthValidationRule.minimumLength + 1)))).isEmpty();
    }

    @Test
    void shouldBeInvalidWhenLengthIsAboveMinimum() {
        assertThat(validationRule.validate(buildAd("a".repeat(AdLastNameLengthValidationRule.minimumLength - 1)))).containsOnly(AdLastNameLengthValidationRule.ruleName);
    }

    @Test
    void shouldBeInvalidWhenLengthIsEqualToMinimum() {
        assertThat(validationRule.validate(buildAd("a".repeat(AdLastNameLengthValidationRule.minimumLength)))).containsOnly(AdLastNameLengthValidationRule.ruleName);
    }

    Ad buildAd(String lastName) {
        return Ad.builder()
                 .contact(Contact.builder()
                                 .lastName(lastName)
                                 .build())
                 .build();
    }
}