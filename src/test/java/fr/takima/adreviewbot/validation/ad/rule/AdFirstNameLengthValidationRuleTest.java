package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.model.Contact;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AdFirstNameLengthValidationRuleTest {

    AdFirstNameLengthValidationRule validationRule = new AdFirstNameLengthValidationRule();

    @Test
    void shouldBeOk() {
        assertThat(validationRule.validate(buildAd("a".repeat(AdFirstNameLengthValidationRule.minimumLength + 1)))).isEmpty();
    }

    @Test
    void shouldBeInvalidWhenLengthIsAboveMinimum() {
        assertThat(validationRule.validate(buildAd("a".repeat(AdFirstNameLengthValidationRule.minimumLength - 1)))).containsOnly(AdFirstNameLengthValidationRule.ruleName);
    }

    @Test
    void shouldBeInvalidWhenLengthIsEqualToMinimum() {
        assertThat(validationRule.validate(buildAd("a".repeat(AdFirstNameLengthValidationRule.minimumLength)))).containsOnly(AdFirstNameLengthValidationRule.ruleName);
    }

    Ad buildAd(String firstName) {
        return Ad.builder()
                 .contact(Contact.builder()
                                 .firstName(firstName)
                                 .build())
                 .build();
    }
}