package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.model.Contact;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AdEmailAlphaRateValidationRuleTest {

    AdEmailAlphaRateValidationRule validationRule = new AdEmailAlphaRateValidationRule();

    @Test
    void shouldBeOk() {
        assertThat(validationRule.validate(buildAd("thisIsValid@email.fr"))).isEmpty();
    }

    @Test
    void shouldBeInvalid() {
        assertThat(validationRule.validate(buildAd("a------------------s@email.fr"))).containsOnly(AdEmailAlphaRateValidationRule.ruleName);
    }

    Ad buildAd(String email) {
        return Ad.builder()
                 .contact(Contact.builder()
                                 .email(email)
                                 .build())
                 .build();
    }
}