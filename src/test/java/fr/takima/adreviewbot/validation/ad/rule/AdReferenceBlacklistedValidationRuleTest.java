package fr.takima.adreviewbot.validation.ad.rule;

import fr.takima.adreviewbot.model.Ad;
import fr.takima.adreviewbot.model.Item;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AdReferenceBlacklistedValidationRuleTest {

    AdReferenceBlacklistedValidationRule validationRule = new AdReferenceBlacklistedValidationRule();

    @Test
    void shouldBeOk() {
        assertThat(validationRule.validate(buildAd("validEan"))).isEmpty();
    }

    @Test
    void shouldBeInvalidForBlacklistedItem() {
        assertThat(validationRule.validate(buildAd("9-782940-199617"))).containsOnly(AdReferenceBlacklistedValidationRule.ruleName);
    }

    Ad buildAd(String eanCode) {
        return Ad.builder()
                 .item(Item.builder()
                           .eanCode(eanCode)
                           .build())
                 .build();
    }
}